import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../_models/user.model';


export interface AuthStatus {
	status: 'ok' | 'none';
	username?: string;
}

export interface AuthLoginData {
	user: string;
	password: string;
	language?: string;
}

export interface AuthLogin {
	token: string;
	username: string;
	email: string;
	usertype: string;
	policyversion: {
		current: { terms: number; privacy: number };
		accepted: { terms: number; privacy: number };
	};
}

export interface ChangePasswordData {
	token?: string;
	oldpassword?: string;
	newpassword: string;
}

export interface AuthSignupData {
	email: string;
	username: string;
	password: string;
	newsletter: boolean;
	refby?: string;
	language?: string;
	terms: boolean;
	usertype: string;
	subtype?: string;
}


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

	login(loginData: AuthLoginData): Observable<AuthLogin> {
		return this.http.post<AuthLogin>(environment.helperbitApi + '/login', loginData);
	}

	activate(email: string, token: string): Observable<void> {
		return this.http.post<void>(environment.helperbitApi + '/auth/activate', { email: email, token: token });
  }

	resendActivation(email: string): Observable<void> {
		return this.http.post<void>(environment.helperbitApi + '/auth/activate/resend', { email: email });
	}

	logout(): Observable<void> {
    // localStorage.removeItem('currentUser');
		return this.http.post<void>(environment.helperbitApi + '/logout', {},
			{ headers: { ignoreLoadingBar: String(true) } });
  }
  
	signup(signupData: AuthSignupData): Observable<void> {
		return this.http.post<void>(environment.helperbitApi + '/signup', signupData);
	}

	changePassword(changeData: ChangePasswordData): Observable<void> {
		return this.http.post<void>(environment.helperbitApi + '/auth/change', changeData);
	}

	resetPassword(email: string): Observable<void> {
		return this.http.post<void>(environment.helperbitApi + '/auth/reset', { email: email });
	}
}
