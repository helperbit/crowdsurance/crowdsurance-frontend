import { TestBed } from '@angular/core/testing';

import { TezosService } from './tezos.service';

describe('TezosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TezosService = TestBed.get(TezosService);
    expect(service).toBeTruthy();
  });
});
