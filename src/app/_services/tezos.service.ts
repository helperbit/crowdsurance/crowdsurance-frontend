import { Injectable, EventEmitter } from '@angular/core';
import { environment } from '../../environments/environment';
import { timer } from 'rxjs';
import { HttpClient } from '@angular/common/http';

export interface ConnectionStatus {
  online: boolean;
  node: string;
  level: number;
  hash: string;
}

@Injectable({
  providedIn: 'root'
})
export class TezosService {
  public connectionStatus: EventEmitter<ConnectionStatus>;

  constructor(private http: HttpClient) {
    this.connectionStatus = new EventEmitter();
    this.connectionStatus.emit({ online: false, node: environment.node, level: 0, hash: '' });

    timer(0, 10000).subscribe(() => this.updateNodeStatus());
    this.updateNodeStatus();
  }

  updateNodeStatus() {
    this.http.get(environment.node + '/chains/main/blocks/head').subscribe((data: any) => {
      this.connectionStatus.emit({ online: true, node: environment.node, level: data.header.level, hash: data.hash });
    }, () => {
      this.connectionStatus.emit({ online: false, node: environment.node, level: 0, hash: '' });
    });
  }
}
