import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BuycoverComponent } from './buycover/buycover.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';


const routes: Routes = [
  {
    component: DashboardComponent,
    path: ''
  },
  {
    component: BuycoverComponent,
    path: 'buy-cover'
  },
  {
    component: RegisterComponent,
    path: 'register',
    data: { fullPage: false }
  },
  {
    component: LoginComponent,
    path: 'login',
    data: { fullPage: false }
  },
  {
    component: ForgotPasswordComponent,
    path: 'forgot-password',
    data: { fullPage: false }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
