import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuycoverComponent } from './buycover.component';

describe('BuycoverComponent', () => {
  let component: BuycoverComponent;
  let fixture: ComponentFixture<BuycoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuycoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuycoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
