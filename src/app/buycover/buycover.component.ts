import { Component, OnInit } from '@angular/core';
import { latLng, tileLayer, Layer, marker, icon } from 'leaflet';

@Component({
  selector: 'app-buycover',
  templateUrl: './buycover.component.html',
  styleUrls: ['./buycover.component.scss']
})
export class BuycoverComponent implements OnInit {
  public mapOptions: any;
  public markers: Layer[] = [];

  constructor() {
    this.markers = [marker(
			[ 46.879966 + 0.1 * (Math.random() - 0.5), -121.726909 + 0.1 * (Math.random() - 0.5) ],
			{
				icon: icon({
					iconSize: [ 25, 41 ],
					iconAnchor: [ 13, 41 ],
					iconUrl: 'assets/marker-icon.png',
					shadowUrl: 'assets/marker-shadow.png'
				})
			}
		)]
    this.mapOptions = {
      layers: [
        tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', { maxZoom: 18, attribution: '...' })
      ],
      zoom: 5,
      center: latLng(46.879966, -121.726909)
    };
  }

  ngOnInit() {
  }

}
