export const environment = {
  production: true,
  network: 'mainnet',
  node: 'https://mainnet.tezrpc.me:443',
  helperbitApi: 'https://api.helperbit.com/api/v1/'
};
